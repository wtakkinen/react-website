import { render, screen } from '@testing-library/react';
import App from './App';

test('renders h2', () => {
  render(<App />);
  const headerElement = screen.getByText(/Ei hakutuloksia/i);
  expect(headerElement).toBeInTheDocument();
});


